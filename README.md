Some micropython for running Adafruit big 7 seg as a display

Uses micropython on an esp32

  mpremote connect /dev/ttyUSB0 fs cp  clock.py :main.py ; mpremote connect /dev/ttyUSB0 repl

expects as file wifi.py with the following:

   sid=<your wifi sid>
   passwd=<your wifi password>

Then just upload all the .py files using mpremote and it shoud Just Work(TM)!

Most of this code comes from https://github.com/smittytone/HT16K33-Python and so their copyright and MIT licence applies there.

Any code I have written here is under [GNU GPL 3.0](https://www.gnu.org/licenses/gpl-3.0.en.html)
