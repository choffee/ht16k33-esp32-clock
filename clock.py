# IMPORTS
import utime as time
from machine import I2C, Pin, RTC, WDT
from ht16k33segmentbig import HT16K33SegmentBig
import ntptime
import network
import wifi
import socket
import re

bst_re = re.compile("\"dst\":true")

def is_summer():
    """Look up if it's summertime, use online service
    https://www.timeapi.io/api/Time/current/zone?timeZone=Europe/London"""
    url =  "http://worldtimeapi.org/api/timezone/Europe/London"
    _, _, host, path = url.split('/', 3)
    addr = socket.getaddrinfo(host, 80)[0][-1]
    s = socket.socket()
    s.connect(addr)
    s.send(bytes('GET /%s HTTP/1.0\r\nHost: %s\r\n\r\n' % (path, host), 'utf8'))
    data_js=""
    while True:
        data = s.recv(100)
        if data:
            print(str(data, 'utf8'), end='')
            data_js += str(data, 'utf8')
        else:
            break
    s.close()
    print(data_js)
    return bst_re.search(data_js)


# START
if __name__ == '__main__':
    i2c = I2C(0, scl=Pin(32), sda=Pin(33))  # Adafruit QTPy RP2040

    display = HT16K33SegmentBig(i2c)
    display.set_brightness(2)

    # Set the watchdog for 10 seconds
    wdt = WDT(timeout=10000)

    # Write 'SYNC' to the LED using custom glyphs
    sync_text = b"\x6D\x6E\x37\x39"
    for i in range(len(sync_text)):
        display.set_glyph(sync_text[i], i)
    display.draw()
    display.set_colon(display.COLON_CENTRE).draw()

    def do_connect():
        "Connect us to the network..."
        wlan = network.WLAN(network.STA_IF)
        wlan.active(True)
        if not wlan.isconnected():
            print('connecting to network...')
            wlan.connect(wifi.sid, wifi.passwd)
            while not wlan.isconnected():
                pass
        print('network config:', wlan.ifconfig())

    do_connect()

    # Assuming we have network now.
    rtc = RTC()
    ntptime.settime()
    print(rtc.datetime())
    summer = is_summer()
    print(summer)
    if summer:
        offset = 1
    else:
        offset = 0

    def display_time():
        "Just get the hours and mins from the current time"
        hourmin = rtc.datetime()[4:6]
        # print(hourmin)
        hr = hourmin[0] + offset
        display.set_number(int(hr / 10), 0)
        display.set_number(hr % 10, 1)
        display.set_number(int(hourmin[1] / 10), 2)
        display.set_number(hourmin[1] % 10, 3)
        display.draw()

    colon = True
    while True:
        display_time()
        if colon:
            display.set_colon(display.COLON_CENTRE).draw()
            colon = False
        else:
            display.set_colon(0x00).draw()
            colon = True
        # Feed the watchdog
        wdt.feed()
        time.sleep(1)
